﻿using FluentAssertions;
using FluentAssertions.Primitives;
using ServiceStack;
using ServiceStack.Text;

namespace NTestHelper
{
    public static class ServiceStackSerializableExtensions
    {
        public static AndConstraint<ObjectAssertions> BeServiceStackSerializable(this ObjectAssertions assertions)
        {
            var json = assertions.Subject.ToJson();
            var clone = JsonSerializer.DeserializeFromString(json, assertions.Subject.GetType());

            clone.Should().BeEquivalentServiceStackJson(assertions.Subject);

            return new AndConstraint<ObjectAssertions>(assertions);
        }
    }
}