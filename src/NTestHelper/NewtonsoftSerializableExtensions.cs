﻿using FluentAssertions;
using FluentAssertions.Primitives;
using JsonNet.ContractResolvers;
using Newtonsoft.Json;

namespace NTestHelper
{
    public static class NewtonsoftSerializableExtensions
    {
        private static readonly JsonSerializerSettings JsonPrivateSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            ContractResolver = new PrivateSetterContractResolver(),
        };

        private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
        };

        public static AndConstraint<ObjectAssertions> BeNewtonsoftSerializable(this ObjectAssertions assertions, bool serializePrivateProperties = false)
        {
            return serializePrivateProperties
                ? BeNewtonsoftSerializable(assertions, JsonPrivateSettings)
                : BeNewtonsoftSerializable(assertions, JsonSettings);
        }

        public static AndConstraint<ObjectAssertions> BeNewtonsoftSerializable(this ObjectAssertions assertions, JsonSerializerSettings jsonSettings)
        {
            var json = JsonConvert.SerializeObject(assertions.Subject, typeof(object), jsonSettings);
            var clone = JsonConvert.DeserializeObject(json, assertions.Subject.GetType(), jsonSettings);

            clone.Should().BeEquivalentNewtonsoftJson(assertions.Subject);

            return new AndConstraint<ObjectAssertions>(assertions);
        }
    }
}