﻿using System.IO;
using System.Reflection;
using FluentAssertions.Equivalency;
using FluentAssertions.Primitives;
using FluentAssertions;
using System.Xml.Serialization;

namespace NTestHelper;

public static class BeXmlSerializableExtensions
{


    public static AndConstraint<ObjectAssertions> BeXmlSerializableExcludingIgnoredMembers(this ObjectAssertions assertions, params string[] excludedPropertyNames)
    {
        using (var memoryStream = new MemoryStream())
        {
            var xmlSerializer = new XmlSerializer(assertions.Subject.GetType());
            xmlSerializer.Serialize(memoryStream, assertions.Subject);
            memoryStream.Position = 0L;
            var result = xmlSerializer.Deserialize(memoryStream);

            result.Should().BeEquivalentToExcludingXmlIgnoreProperty(assertions.Subject);
        }

        return new AndConstraint<ObjectAssertions>(assertions);
    }

    public static AndConstraint<ObjectAssertions> BeXmlSerializableExcludingXmlIgnoreProperty(this ObjectAssertions assertions)
    {
        using (var memoryStream = new MemoryStream())
        {
            var xmlSerializer = new XmlSerializer(assertions.Subject.GetType());
            xmlSerializer.Serialize(memoryStream, assertions.Subject);
            memoryStream.Position = 0L;
            var result = xmlSerializer.Deserialize(memoryStream);

            result.Should().BeEquivalentToExcludingXmlIgnoreProperty(assertions.Subject);
        }

        return new AndConstraint<ObjectAssertions>(assertions);
    }


}