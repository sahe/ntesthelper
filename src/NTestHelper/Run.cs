﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace NTestHelper
{
    public static class Run
    {
        /// <summary>
        /// Runs given Action inside a new STA thread.
        /// </summary>
        /// <remarks>https://github.com/xunit/xunit/issues/103</remarks>
        /// <param name="action"></param>
        public static Task StaThreadAsync(Action action)
        {
            var tcs = new TaskCompletionSource<object>();
            var thread = new Thread(() =>
            {
                try
                {
                    action();
                    tcs.SetResult(new object());
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return tcs.Task;
        }
    }
}