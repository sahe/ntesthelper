﻿using System.Diagnostics;
using System.Reflection;
using System.Xml.Serialization;
using FluentAssertions;
using FluentAssertions.Equivalency;
using FluentAssertions.Primitives;
using Newtonsoft.Json;
using ServiceStack;

namespace NTestHelper
{
    public static class BeEquivalentToExtensions
    {
        public static AndConstraint<ObjectAssertions> BeEquivalentToExcludingXmlIgnoreProperty(this ObjectAssertions assertions, object source)
        {
            source.Should().BeEquivalentTo(assertions.Subject, m => m.Excluding(n => CheckForXmlIgnoreAttribute(n)));

            return new AndConstraint<ObjectAssertions>(assertions);
        }

        public static bool CheckForXmlIgnoreAttribute(IMemberInfo memberInfo)
        {
            return memberInfo.DeclaringType
                .GetProperty(memberInfo.Name)
                ?.GetCustomAttribute<XmlIgnoreAttribute>() is not null;
        }

        public static AndConstraint<ObjectAssertions> BeEquivalentNewtonsoftJson(this ObjectAssertions assertions, object expected, JsonSerializerSettings jsonSerializerSettings = null)
        {
            var givenJson = JsonConvert.SerializeObject(assertions.Subject, typeof(object), jsonSerializerSettings);
            var expectedJson = JsonConvert.SerializeObject(expected, typeof(object), jsonSerializerSettings);
            Debug.WriteLine($"givenCloneable:    {givenJson}");
            Debug.WriteLine($"expectedCloneable: {expectedJson}");
            givenJson.Should().Be(expectedJson);

            return new AndConstraint<ObjectAssertions>(assertions);
        }

        public static AndConstraint<ObjectAssertions> BeEquivalentServiceStackJson(this ObjectAssertions assertions, object expected)
        {
            var givenJson = assertions.Subject.ToJson();
            var expectedJson = expected.ToJson();
            givenJson.Should().Be(expectedJson);

            return new AndConstraint<ObjectAssertions>(assertions);
        }

        public static AndConstraint<ObjectAssertions> BeEquivalentSystemTextJson(this ObjectAssertions assertions, object expected)
        {
            var givenJson = System.Text.Json.JsonSerializer.Serialize(assertions.Subject, typeof(object));
            var expectedJson = System.Text.Json.JsonSerializer.Serialize(expected, typeof(object));
            givenJson.Should().Be(expectedJson);

            return new AndConstraint<ObjectAssertions>(assertions);
        }
    }
}