﻿using System.Text.Json;
using FluentAssertions;
using FluentAssertions.Primitives;

namespace NTestHelper
{
    public static class SystemTextJsonSerializableExtensions
    {
        public static JsonSerializerOptions DefaultJsonSerializerOptions = new JsonSerializerOptions()
        {
        };

        public static AndConstraint<ObjectAssertions> BeSystemTextJsonSerializable(this ObjectAssertions assertions, JsonSerializerOptions jsonOptions)
        {
            var json = JsonSerializer.Serialize(assertions.Subject, typeof(object), jsonOptions);
            var clone = JsonSerializer.Deserialize(json, assertions.Subject.GetType(), jsonOptions);

            clone.Should().BeEquivalentSystemTextJson(assertions.Subject);

            return new AndConstraint<ObjectAssertions>(assertions);
        }

        public static AndConstraint<ObjectAssertions> BeSystemTextJsonSerializable(this ObjectAssertions assertions)
            => BeSystemTextJsonSerializable(assertions, DefaultJsonSerializerOptions);
    }
}