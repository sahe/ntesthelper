﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using FluentAssertions;

namespace NTestHelper
{
    public static class PropertyTestExtension
    {
        public static T GetShouldEqualSet<T, TProperty>(this T owner, Expression<Func<T, TProperty>> propertyPicker, TProperty newValue)
        {
            return GetShouldEqualSet(owner, propertyPicker, newValue, expectPropertyChanged: false);
        }

        public static T GetShouldEqualSetAndNotify<T, TProperty>(this T owner, Expression<Func<T, TProperty>> propertyPicker, TProperty newValue)
        {
            return GetShouldEqualSet(owner, propertyPicker, newValue, expectPropertyChanged: true);
        }

        public static T GetShouldEqualSet<T, TProperty>(this T owner, Expression<Func<T, TProperty>> propertyPicker, TProperty newValue, bool expectPropertyChanged)
        {
            // arrange
            var property = (PropertyInfo)((MemberExpression)propertyPicker.Body).Member;
            var startValue = property.GetValue(owner);
            if (object.Equals(startValue, newValue))
                throw new ArgumentException($"Given parameter 'newValue'={newValue} which equals the current value of {typeof(T).Name}.{typeof(TProperty).Name}. Please choose another value for parameter 'newValue'.");
            var notified = false;

            // act
            if (owner is INotifyPropertyChanged inpc)
            {
                inpc.PropertyChanged += (s, e) => notified = true;

                property.SetValue(owner, newValue);
            }
            else
            {
                property.SetValue(owner, newValue);
            }
            var finalValue = property.GetValue(owner);

            // Undo
            property.SetValue(owner, startValue);

            // assert
            finalValue.Should().Be(newValue, "A property getter should return exactly what has been set before.");
            notified.Should().Be(expectPropertyChanged, "NotifyPropertyChanged behavior is not as expected.");

            return owner;
        }
    }
}