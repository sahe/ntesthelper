﻿using System;
using System.ComponentModel;
using FluentAssertions;

namespace NTestHelper
{
    /// <summary>
    /// http://blog.ploeh.dk/2009/08/06/AFluentInterfaceForTestingINotifyPropertyChanged/
    /// </summary>
    public class NotifyExpectation<T>
    {
        private readonly bool _eventExpected;

        private readonly T _owner;

        private readonly string _propertyName;

        public NotifyExpectation(T owner, string propertyName, bool eventExpected)
        {
            _owner = owner;
            _propertyName = propertyName;
            _eventExpected = eventExpected;
        }

        public void When(Action<T> action)
        {
            var eventWasRaised = false;

            ((INotifyPropertyChanged)_owner).PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName == _propertyName)
                {
                    eventWasRaised = true;
                }
            };
            action(_owner);

            _eventExpected.Should().Be(eventWasRaised, $"PropertyChanged on {nameof(T)}.{_propertyName}");
        }
    }
}