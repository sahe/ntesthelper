﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;

namespace NTestHelper;

public static class DumpExtension
{
    /// <summary>
    /// Returns a nicely formatted JSON string that represents the given source:
    /// </summary>
    public static string DumpJson(this object source)
    {
        var setting = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            TypeNameHandling = TypeNameHandling.All,
        };

        return JsonConvert.SerializeObject(source, setting);
    }

    /// <summary>
    /// Returns the ToString() result of the given object(s).
    /// If this object(s) contain properties that can be casted to <see cref="IEnumerable"/>,
    /// these sub-objects are appended to the return string.
    /// </summary>
    /// <remarks>This method makes use of reflection and is expected to have a poor runtime performance.</remarks>
    public static string DumpToString(this object source)
    {
        return DumpToString(source, m => true);
    }

    /// <summary>
    /// Returns the ToString() result of the given object(s).
    /// If this object(s) contain properties that can be casted to <see cref="IEnumerable"/>,
    /// these sub-objects are appended to the return string.
    /// </summary>
    /// <remarks>This method makes use of reflection and is expected to have a poor runtime performance.</remarks>
    public static string DumpToString(this object source, Predicate<object> predicate)
    {
        if (source == null)
            return "null";

        var retval = new StringBuilder();

        if (source is IEnumerable enumerable)
        {
            foreach (var item in enumerable)
            {
                DumpToString(retval, item, predicate, indent: 0);
            }
        }
        else
        {
            DumpToString(retval, source, predicate, indent: 0);
        }

        return retval.ToString();
    }

    private static void DumpToString(StringBuilder result, object source, Predicate<object> predicate, int indent)
    {
        if (predicate(source))
            result.AppendLine(new string(' ', indent * 2) + source.ToString());

        var props = source.GetType()
            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Where(m => m.GetIndexParameters().Length == 0 // skip indexed properties
                        && typeof(IEnumerable).IsAssignableFrom(m.PropertyType)
                        && m.PropertyType != typeof(string))
            .ToArray();

        foreach (var prop in props)
        {
            if (prop.GetValue(source) is IEnumerable enumerable)
            {
                var nextIndexnt = indent + 1;
                foreach (var item in enumerable)
                {
                    DumpToString(result, item, predicate, nextIndexnt);
                }
            }
        }
    }
}