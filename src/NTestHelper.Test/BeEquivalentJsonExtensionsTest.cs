﻿using System.Runtime.Serialization;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTestHelper.Test
{
    [TestClass]
    public class BeEquivalentJsonExtensionsTest
    {
        [TestMethod]
        public void NestedTest()
        {
            // arrange
            var sut1 = new NestedModel
            {
                Child = new SimpleModel
                {
                    Ignored = 88,
                    NotIgnored = 99,
                }
            };

            var sut2 = new NestedModel
            {
                Child = new SimpleModel
                {
                    Ignored = 888,
                    NotIgnored = 99,
                }
            };

            // act && assert
            sut1.Should().BeEquivalentServiceStackJson(sut2);
        }

        [TestMethod]
        public void SimpleTest()
        {
            // arrange
            var sut1 = new SimpleModel
            {
                Ignored = 88,
                NotIgnored = 99,
            };

            var sut2 = new SimpleModel
            {
                Ignored = 888,
                NotIgnored = 99,
            };

            // act && assert
            sut1.Should().BeEquivalentServiceStackJson(sut2);
        }

        private class NestedModel
        {
            public SimpleModel Child { get; set; }
        }

        private class SimpleModel
        {
            [IgnoreDataMember]
            public int Ignored { get; set; }

            public int NotIgnored { get; set; }
        }
    }
}