﻿using System;
using System.Runtime.Serialization;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTestHelper.Test
{
    [TestClass]
    public class ServiceStackSerializableExtensionsTest
    {
        [TestMethod]
        public void ClassFail1Test()
        {
            var test = new FailModel1
            {
                MyProperty = 88,
                MyProperty2 = "foo",
            };

            new Action(() => test.Should().BeServiceStackSerializable()).Should()
                .Throw<AssertFailedException>();
        }

        [TestMethod]
        public void ClassFail2Test()
        {
            var test = new FailModel2(88)
            {
                MyProperty2 = "foo",
            };

            test.Should().BeServiceStackSerializable();
        }

        [TestMethod]
        public void ClassTest()
        {
            var test = new TestModel("private")
            {
                MyProperty = 88,
                MyProperty2 = "foo",
                IgnoredString = "this must be ignored",
            };

            test.Should().BeServiceStackSerializable();
        }

        [TestMethod]
        public void IntTest()
        {
            object test = "88";

            test.Should().BeServiceStackSerializable();
        }

        [TestMethod]
        public void StringTest()
        {
            object test = "abc";

            test.Should().BeServiceStackSerializable();
        }

        private class FailModel1
        {
            private int myVar;

            public int MyProperty
            {
                get { return myVar + 1; }
                set { myVar = value; }
            }

            public string MyProperty2 { get; set; }
        }

        private class FailModel2
        {
            public FailModel2(int myProperty)
            {
                MyProperty = myProperty;
            }

            public FailModel2(string myProperty2)
            {
                MyProperty2 = myProperty2;
            }

            public int MyProperty { get; set; }

            public string MyProperty2 { get; set; }
        }

        private class TestModel
        {
            public TestModel()
            {
            }

            public TestModel(string privateSet)
            {
                PrivateSetProperty = privateSet;
            }

            [IgnoreDataMember]
            public string IgnoredString { get; set; }

            public int MyProperty { get; set; }

            public string MyProperty2 { get; set; }

            public string PrivateSetProperty { get; private set; }
        }
    }
}