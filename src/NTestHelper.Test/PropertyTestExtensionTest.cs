﻿using System;
using System.ComponentModel;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTestHelper.Test
{
    [TestClass]
    public class PropertyTestExtensionTest
    {
        [TestMethod]
        public void GetShouldEqualSetAndNotifyTest()
        {
            // arrange
            var target = new NpcModel();

            // act && assert
            new Action(() => target.GetShouldEqualSetAndNotify(m => m.IntProp, 0)).Should().Throw<ArgumentException>();
            new Action(() => target.GetShouldEqualSetAndNotify(m => m.NoNotifyProp, 0)).Should().Throw<ArgumentException>();
            target.GetShouldEqualSetAndNotify(m => m.IntProp, 88);
        }

        [TestMethod]
        public void GetShouldEqualSetTest()
        {
            // arrange
            var target = new TestModel();

            // act && assert
            new Action(() => target.GetShouldEqualSet(m => m.IntProp, 0)).Should().Throw<ArgumentException>();
            target.GetShouldEqualSet(m => m.IntProp, 88);

            new Action(() => target.GetShouldEqualSet(m => m.StringProp, null)).Should().Throw<ArgumentException>();
            target.GetShouldEqualSet(m => m.StringProp, "foo");

            new Action(() => target.GetShouldEqualSet(m => m.DtProp, DateTime.MinValue)).Should().Throw<ArgumentException>();
            target.GetShouldEqualSet(m => m.DtProp, DateTime.Now);
        }

        private class NpcModel : INotifyPropertyChanged
        {
            private int myVar;

            public event PropertyChangedEventHandler PropertyChanged;

            public int IntProp
            {
                get
                {
                    return myVar;
                }

                set
                {
                    myVar = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IntProp)));
                }
            }

            public int NoNotifyProp { get; set; }
        }

        private class TestModel
        {
            public DateTime DtProp { get; set; }

            public int IntProp { get; set; }

            public string StringProp { get; set; }
        }
    }
}