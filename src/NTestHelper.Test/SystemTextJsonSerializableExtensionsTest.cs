﻿using System;
using System.Text.Json.Serialization;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTestHelper.Test
{
    [TestClass]
    public class SystemTextJsonSerializableExtensionsTest
    {
        [TestMethod]
        public void ClassFail1Test()
        {
            var test = new FailModel1
            {
                MyProperty = 88,
                MyProperty2 = "foo",
            };

            new Action(() => test.Should().BeSystemTextJsonSerializable()).Should()
                .Throw<AssertFailedException>();
        }

        [TestMethod]
        public void ClassFail2Test()
        {
            var test = new FailModel2(7)
            {
                MyProperty = 88,
                MyProperty2 = "foo",
            };

            new Action(() => test.Should().BeSystemTextJsonSerializable()).Should()
                .Throw<NotSupportedException>();
        }

        [TestMethod]
        public void ClassTest()
        {
            var test = new TestModel()
            {
                MyProperty = 88,
                MyProperty2 = "foo",
                IgnoredString = "this must be ignored",
            };

            test.Should().BeSystemTextJsonSerializable();
        }

        [TestMethod]
        public void IntTest()
        {
            object test = "88";

            test.Should().BeSystemTextJsonSerializable();
        }

        [TestMethod]
        public void StringTest()
        {
            object test = "abc";

            test.Should().BeSystemTextJsonSerializable();
        }

        private class FailModel1
        {
            private int myVar;

            public int MyProperty
            {
                get { return myVar + 1; }
                set { myVar = value; }
            }

            public string MyProperty2 { get; set; }
        }

        private class FailModel2
        {
            public FailModel2(int a)
            {
            }

            public FailModel2(string a)
            {
            }

            public int MyProperty { get; set; }

            public string MyProperty2 { get; set; }
        }

        private class TestModel
        {
            public TestModel()
            {
            }

            [JsonIgnore]
            public string IgnoredString { get; set; }

            public int MyProperty { get; set; }

            public string MyProperty2 { get; set; }
            // private or readonly properties are not supported
            // public string PrivateSetProperty { get; }
        }
    }
}