﻿using System.Collections.Generic;
using System.Diagnostics;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTestHelper.Test
{
    [TestClass]
    public class DumpExtensionTest
    {
        [TestMethod]
        public void DumpToStringFilteredTest()
        {
            // arrange
            var model = new TestModel
            {
                Children = new List<TestModel>
                {
                    new TestModel
                    {
                        Children = new List<TestModel>
                        {
                            new TestModel(),
                            new ChildTestModel(),
                        }
                    },
                    new ChildTestModel
                    {
                        Children = new List<TestModel>
                        {
                            new TestModel(),
                        }
                    },
                }
            };

            // act
            var dump = model.DumpToString(m => m.GetType() != typeof(ChildTestModel));

            // assert
            Debug.WriteLine(dump);
            dump.Should().BeEquivalentTo("TestModel\r\n  TestModel\r\n    TestModel\r\n    TestModel\r\n");
        }

        [TestMethod]
        public void DumpToStringTest()
        {
            // arrange
            var model = new TestModel
            {
                Children = new List<TestModel>
                {
                    new TestModel
                    {
                        Children = new List<TestModel>
                        {
                            new TestModel(),
                            new ChildTestModel(),
                        }
                    },
                    new ChildTestModel
                    {
                        Children = new List<TestModel>
                        {
                            new TestModel(),
                        }
                    },
                }
            };

            // act
            var dump = model.DumpToString();

            // assert
            Debug.WriteLine(dump);
            dump.Should().BeEquivalentTo("TestModel\r\n  TestModel\r\n    TestModel\r\n    ChildTestModel\r\n  ChildTestModel\r\n    TestModel\r\n");
        }

        private class ChildTestModel : TestModel
        {
            public override string ToString()
            {
                return $"{nameof(ChildTestModel)}";
            }
        }

        private class TestModel
        {
            public List<TestModel> Children { get; set; }

            public override string ToString()
            {
                return $"{nameof(TestModel)}";
            }
        }
    }
}