﻿using System;
using System.Runtime.Serialization;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace NTestHelper.Test
{
    [TestClass]
    public class NewtonsoftSerializableExtensionsTest
    {
        [TestMethod]
        public void ClassFail1Test()
        {
            var test = new FailModel1
            {
                MyProperty = 88,
                MyProperty2 = "foo",
            };

            new Action(() => test.Should().BeNewtonsoftSerializable()).Should()
                .Throw<AssertFailedException>();
        }

        [TestMethod]
        public void ClassFail2Test()
        {
            var test = new FailModel2(7)
            {
                MyProperty = 88,
                MyProperty2 = "foo",
            };

            new Action(() => test.Should().BeNewtonsoftSerializable()).Should()
                .Throw<JsonSerializationException>()
                .WithMessage("Unable to find a constructor to use*");
        }

        [TestMethod]
        public void ClassTest()
        {
            var test = new TestModel("private")
            {
                MyProperty = 88,
                MyProperty2 = "foo",
                IgnoredString = "this must be ignored",
            };

            test.Should().BeNewtonsoftSerializable(serializePrivateProperties: true);

            // Optionally, provide json serializer settings
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None,
            };

            new Action(() => test.Should().BeNewtonsoftSerializable(settings)).Should().Throw<Exception>("Because private setters are not supported by given settings.");
        }

        [TestMethod]
        public void IntTest()
        {
            object test = "88";

            test.Should().BeNewtonsoftSerializable();
        }

        [TestMethod]
        public void StringTest()
        {
            object test = "abc";

            test.Should().BeNewtonsoftSerializable();
        }

        public class FailModel1
        {
            private int myVar;

            public int MyProperty
            {
                get { return myVar + 1; }
                set { myVar = value; }
            }

            public string MyProperty2 { get; set; }
        }

        public class FailModel2
        {
            public FailModel2(int a)
            {
            }

            public FailModel2(string a)
            {
            }

            public int MyProperty { get; set; }

            public string MyProperty2 { get; set; }
        }

        public class TestModel
        {
            public TestModel()
            {
            }

            public TestModel(string privateSet)
            {
                PrivateSetProperty = privateSet;
            }

            [IgnoreDataMember]
            public string IgnoredString { get; set; }

            public int MyProperty { get; set; }

            public string MyProperty2 { get; set; }

            public string PrivateSetProperty { get; private set; }
        }
    }
}