# NTestHelper

[Hosted on nuget](https://www.nuget.org/packages/NTestHelper/)
Install-Package NTestHelper

**Remember to manually install nuget package 'xunit.runner.visualstudio' if you use VisualStudios Test Explorer.**

## Howto test INotifyPropertyChanged

```C#
[TestMethod]
public void PropertyChangedTest()
    // arrange
    var mock = new AutoMocker();
    var target = mock.CreateInstance<MyViewModel>();
    
    // act + assert
    target.ShouldNotifyOn(m => m.CanClose).When(m => m.CanClose = !m.CanClose);
}

```    
## Howto test `normal  property getter and setter

The following codes sets verifies, that the property getter returns exactly what has been set before.

```C#
[TestMethod]
public void GetShouldEqualSetTest()
{
    // arrange
    var target = new MyModel();

    // act && assert
    target.GetShouldEqualSet(m => m.IntProp, 88);
    target.GetShouldEqualSet(m => m.DtProp, DateTime.Now);
}
```    

## Howto test if an object is serializable via Newtonsoft.Json

```C#
[TestMethod]
public void SerializableTest()
{
    var test = new TestModel
    {
        MyProperty = 88,
        MyProperty2 = "foo",
    };

    test.Should().BeNewtonsoftSerializable();

    // Optionally, provide json serializer settings
    var settings = new JsonSerializerSettings
    {
        TypeNameHandling = TypeNameHandling.None,
    };

    // test.Should().BeNewtonsoftSerializable(serializePrivateProperties: true);
    test.Should().BeNewtonsoftSerializable(settings);
}
```

## Howto test UI Elements

```C#
[TestMethod]
public async Task Test()
{
    await Run.StaThreadAsync(TestCore);
}

private void TestCore()
{
    // arrange
    var target = new FrameworkElement();
    var red = new SolidColorBrush(Colors.Red);

    // ...
}
```

## Howto dump an object to console

Snippet of DumpExtensionTest.cs
```C#
[TestMethod]
public void DumpToStringTest()
{
    // arrange
    var model = new TestModel
    {
        Children = new List<TestModel>
        {
            new ChildTestModel
            {
                Children = new List<TestModel>
                {
                    new TestModel(),
                }
            },
        }
    };

    // act
    Debug.WriteLine(model.DumpToString());
    // Prints 
    // TestModel
    //   ChildTestModel
    //     TestModel
}
```